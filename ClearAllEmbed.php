<?php

/**
 * Supprime tous les caches oembed de la table postmeta.
 */
class ClearAllEmbed
{
    const PAGE = 'clear-all-oembed';
    const ACTION = 'clear-all-oembed';

    public function __construct()
    {
        add_action('admin_action_' . self::ACTION, [$this, 'action']);
        add_action('admin_menu', function () {
            add_management_page('Clear all OEmbed', 'Clear all OEmbed', 'manage_options', self::PAGE, [$this, 'page']);
        });
    }

    function action()
    {
        global $wpdb;
        $table = $wpdb->prefix . 'postmeta';
        $wpdb->query('DELETE from ' . $table . ' where meta_key LIKE "_oembed%"');
        wp_redirect(admin_url('tools.php?page=' . self::PAGE));
        exit();
    }

    function page()
    {
        global $wpdb;
        $table = $wpdb->prefix . 'postmeta';

        $count = $wpdb->get_var('SELECT count(*) from ' . $table . ' where meta_key LIKE "_oembed%"');

        echo '<h2>Clearing all OEmbed cache</h2>';

        if( $count > 0 )
        {
            echo '
                <p>Actually '.$count.' OEmbed rows.</p>
                <a class="button button-primary" href="' . admin_url('tools.php') . '?action=' . self::ACTION . '">Clear</a>
            ';

        }
        else
        {
            echo '<p>OEmbed cache is empty.</p>';
        }
    }
}

new ClearAllEmbed();
