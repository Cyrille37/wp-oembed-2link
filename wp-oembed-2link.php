<?php
/*
Plugin Name: wp-oembed-2link
Plugin URI: https://framagit.org/Cyrille37/wp-tarteaucitron-lonely/
Description: force oembed to link with thumbnail (GDPR/RGPD)
Version: 1.0.0
Text Domain: wp-oembed-2link
Domain Path: /languages/
Author: Cyrille37
Author URI: https://framagit.org/Cyrille37/
Licence: GPLv3
*/
defined('ABSPATH') or die('No script kiddies please!');

require_once(__DIR__ . '/libraries/action-scheduler/action-scheduler.php');

if (is_blog_admin()) {
    require(__DIR__ . '/ClearAllEmbed.php');
}

class WpOembed2link
{
    const PLUGIN_NAME = 'wp-oembed-2link';
    const ASYNC_ACTION_DOWNLOAD_THUMBNAIL = self::PLUGIN_NAME . '_download-thumbnail';

    public $plugin_path;
    public $plugin_url;

    public $cache_path;
    public $cache_url;

    const SERVICE_YOUTUBE = 'YouTube';
    const SERVICE_DAILYMOTION = 'Dailymotion';
    const SERVICE_VIMEO = 'Vimeo';

    public function __construct()
    {
        $this->plugin_path = realpath(plugin_dir_path(__FILE__)) . '/';
        $this->plugin_url = plugins_url(self::PLUGIN_NAME);

        $this->cache_path = WP_CONTENT_DIR . DIRECTORY_SEPARATOR . 'cache' . DIRECTORY_SEPARATOR . self::PLUGIN_NAME;
        $this->cache_url = content_url('cache' . DIRECTORY_SEPARATOR . self::PLUGIN_NAME);

        add_action('wp_enqueue_scripts', [$this, 'wp_enqueue_scripts'], 1);

        add_action('init', [$this, 'oembed_register_handlers']);
        add_filter('embed_oembed_html', [$this, 'filter_embed_oembed_html'], 1, 4);
        add_filter('oembed_dataparse', [$this, 'filter_oembed_dataparse'], 1, 3);
        add_filter('oembed_ttl', [$this, 'filter_oembed_ttl'], 1, 1);
        add_filter('embed_oembed_discover', [$this, 'filter_embed_oembed_discover'], 1, 1);

        add_action(self::ASYNC_ACTION_DOWNLOAD_THUMBNAIL, [$this, 'async_action_download_thumbnail']);
    }

    public function wp_enqueue_scripts()
    {
        wp_register_style(self::PLUGIN_NAME . '-css', $this->plugin_url . '/style.css');
        wp_enqueue_style(self::PLUGIN_NAME . '-css');
    }

    /**
     * Async job for thumbnail download.
     * @param array $args must contains: 'service', ...
     * 
     * Action scheduler
     * @link https://actionscheduler.org/api/
     * 
     * Run cron with command line
     * `wp-cli cron event run --due-now`
     * @link https://developer.wordpress.org/cli/commands/cron/event/
     * 
     */
    function async_action_download_thumbnail($args)
    {

        if (!isset($args['service']))
            throw new \InvalidArgumentException('"service" argument is mandatory');

        //error_log(__FUNCTION__ . ' args: ' . var_export($args, true));

        if (!is_dir($this->cache_path))
            mkdir($this->cache_path, 0777, true);

        $file_name = $args['thumbnail_filename'];

        $url = null;
        switch ($args['service']) {
            case self::SERVICE_YOUTUBE:
                //$url = 'https://i.ytimg.com/vi/' . $args['videoId'] . '/maxresdefault.jpg';
                /*$url = 'https://www.youtube.com/oembed?url=' . urlencode('https://youtube.com/watch?v=' . $args['videoId']) . '&format=json';
                $content = file_get_contents($url);
                $url = json_decode($content);
                $url = $url->thumbnail_url;*/
                $url = $args['thumbnail_url'];
                break;
            case self::SERVICE_DAILYMOTION:
                //$url = 'https://www.dailymotion.com/thumbnail/video/' . $args['videoId'];
                /*$url = 'https://api.dailymotion.com/video/' . $args['videoId'] . '?fields=thumbnail_480_url';
                $content = file_get_contents($url);
                $url = json_decode($content);
                $url = $url->thumbnail_480_url;*/
                $url = $args['thumbnail_url'];
                break;
            case self::SERVICE_VIMEO:
                /*$url = 'https://vimeo.com/api/v2/video/' . $args['videoId'] . '.json';
                $content = file_get_contents($url);
                $url = json_decode($content);
                $url = $url[0]->thumbnail_large;*/
                $url = $args['thumbnail_url'];
                break;
            case 'prezi':
                $url = 'https://prezi.com/p/' . $args['videoId'];
                $content = file_get_contents($url);
                if (!preg_match('#<meta property="og:image" content="(.*)" />#U', $content, $m))
                    throw new \RuntimeException('Failed to find image for Prezi');
                $url = $m[1];
                break;
        }
        if (!$url)
            return;

        $content = file_get_contents($url);
        file_put_contents(
            $file_name,
            $content
        );
    }

    /**
     * https://oembed.com/
     */
    public function oembed_register_handlers()
    {
    }

    public function filter_oembed_ttl($ttl)
    {
        //error_log(__METHOD__ . ' ttl:' . $ttl);
        //error_log(__METHOD__.' ttl:'.$ttl.' wp_embed:'.var_export($GLOBALS['wp_embed'],true));
        //$ttl = 0;
        return $ttl;
    }

    public function filter_embed_oembed_discover($discover)
    {
        //error_log(__METHOD__ . ' ' . var_export($discover, true));
        return $discover;
    }

    /**
     * for known wordpress oembed handlers
     * @link https://developer.wordpress.org/reference/hooks/embed_oembed_html/
     */
    public function filter_embed_oembed_html($cache, $url, $attr, $post_ID)
    {
        if (is_admin() || isset($_GET['fl_builder'])) {
            return;
        }
        $html = '';
        return empty($html) ? $cache : $html;
    }

    /**
     * @link https://developer.wordpress.org/reference/hooks/oembed_dataparse/
     */
    public function filter_oembed_dataparse($return, $data, $url)
    {
        error_log(__METHOD__ . ' url:' . $url);
        //error_log(__METHOD__ . ' return:' . var_export($return, true));
        //error_log(__METHOD__ . ' data:' . var_export($data, true));

        $url = esc_url($url);
        $url_parse = wp_parse_url($url);
        $host = str_replace(['www.', 'player.'], '', $url_parse['host']);
        // error_log(__METHOD__.' host:'.$host);

        // Rewrite some url

        if ($host == 'youtu.be') {
            preg_match('#.*/([a-zA-Z0-9]+)$#', $url, $m);
            $url = 'https://www.youtube.com/watch?v=' . $m[1];
        }

        $html = null ;
        if ($data && isset($data->provider_name)) {
            switch ($data->provider_name) {
                case self::SERVICE_YOUTUBE:
                    $html = $this->process_youtube($url, $data);
                    break;
                case self::SERVICE_DAILYMOTION:
                    $html = $this->process_dailymotion($url, $data);
                    break;
                case self::SERVICE_VIMEO:
                    $html = $this->process_wimeo($url, $data);
                    break;
            }
        }

        if( empty($html) )
        {
            $html = '<a href="'.$url.'" target="_blank">'.$url.'</a>';
        }

        return empty($html) ? $return : $html;
    }

    protected function process_wimeo(&$url, &$data)
    {
        //error_log(__METHOD__ . ' url:' . $url);
        /* data:(object) array(
            'type' => 'video',
            'version' => '1.0',
            'provider_name' => 'Vimeo',
            'provider_url' => 'https://vimeo.com/',
            'title' => 'Still Life | Conner Griffith 2022',
            'author_name' => 'Conner Griffith',
            'author_url' => 'https://vimeo.com/connergriffith',
            'is_plus' => '0',
            'account_type' => 'pro',
            'html' => '<iframe src="https://player.vimeo.com/video/792716788?h=1d6e53e614&amp;dnt=1&amp;app_id=122963" width="1080" height="608" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen title="Still Life | Conner Griffith 2022"></iframe>',
            'width' => 1080,
            'height' => 608,
            'duration' => 412,
            'description' => 'Composed of over 1000 engravings from the 19th century, Still Life is a meditation on subject/object dualism. The film explores the idea that we live in a world of objects and a world of objects lives within us. Working with this encyclopedia of prints as a sort of language, a story of consciousness emerges.

            DOWNLOAD the full Still Life Object Library (1400+ images!):
            https://drive.google.com/drive/folders/1kZgqyYAoSA_2q8ocBb-Swwo8U4bcAJ3m?usp=sharing

            Director/Animator/Sound Designer
            Conner Griffith
            www.connergriffith.com

            Re-recording Mixer
            Becket Cerny
            www.becketcerny.com

            Sound & Music Consultation
            Alex Mansour
            www.alexmansourmusic.com
            Eamon Griffith
            www.instagram.com/eamongriffith

            The Well Tempered Clavier Prelude No. 1 in C Major
            Performed by Kimiko Ishizaka
            kimikoishizaka.bandcamp.com',
            'thumbnail_url' => 'https://i.vimeocdn.com/video/1610104163-677e026fc47a7815e93e5e6d24a0f7b977a133a0f35c47cf32ccacc79f9f3778-d_960',
            'thumbnail_width' => 960,
            'thumbnail_height' => 540,
            'thumbnail_url_with_play_button' => 'https://i.vimeocdn.com/filter/overlay?src0=https%3A%2F%2Fi.vimeocdn.com%2Fvideo%2F1610104163-677e026fc47a7815e93e5e6d24a0f7b977a133a0f35c47cf32ccacc79f9f3778-d_960&src1=http%3A%2F%2Ff.vimeocdn.com%2Fp%2Fimages%2Fcrawler_play.png',
            'upload_date' => '2023-01-25 13:49:52',
            'video_id' => 792716788,
            'uri' => '/videos/792716788',
            )*/

        $service = self::SERVICE_VIMEO;

        if (!preg_match('#/([0-9]+)(\?.*)?$#', $url, $m) || !isset($m[1]))
            return null;
        $videoId = $m[1];
        $thumbnail_filename = $this->cache_path . DIRECTORY_SEPARATOR . $service . '-' . $videoId . '.jpg';
        $thumbnail_url = $this->cache_url . '/' .  $service . '-' . $videoId . '.jpg';

        $job_args = array('service' => $service, 'videoId' => $videoId, 'thumbnail_filename' => $thumbnail_filename, 'thumbnail_url' => $data->thumbnail_url);
        //if (!as_next_scheduled_action(self::ASYNC_ACTION_DOWNLOAD_THUMBNAIL, ['job_args' => $job_args], self::PLUGIN_NAME)) {
        as_enqueue_async_action(self::ASYNC_ACTION_DOWNLOAD_THUMBNAIL, ['job_args' => $job_args], self::PLUGIN_NAME);
        //}

        // HTML to render (will be stored in oemabed WP cache)

        $title = htmlentities($data->title . ' - ' . $data->author_name, ENT_COMPAT);
        $html = ''
            . '<div class="wp-oembed-2link wp-oembed-2link-service-' . $service . '">'
            . ' <img src="' . $thumbnail_url . '" alt="' . $title . '" />'
            . ' <div class="wp-oembed-2link-menu">'
            . '  <span aria-hidden="true">' . $title . '</span><br/> '
            . '  <a class="" href="' . $url . '" target="_blank">Voir sur Vimeo</a>'
            . ' </div>'
            . '</div>';

        return $html;
    }

    protected function process_dailymotion(&$url, &$data)
    {
        //error_log(__METHOD__ . ' url:' . $url);
        /* data:(object) array(
            'type' => 'video',
            'version' => '1.0',
            'provider_name' => 'Dailymotion',
            'provider_url' => 'https://www.dailymotion.com',
            'title' => 'Présentation du Pôle Ressource Handicap  d\'Indre et Loire',
            'description' => 'Vidéo de présentation du pole ressources "handicap" (enfance : loisirs)  du Département d\'Indre et Loir dont la gestion est assurée par par ACHIL et l\'APAJH.',
            'author_name' => 'Caf Touraine',
            'author_url' => 'https://www.dailymotion.com/caftouraine-quinzaineparentalite',
            'width' => 956,
            'height' => 537,
            'html' => '<iframe frameborder="0" width="956" height="537" src="https://www.dailymotion.com/embed/video/x4muspr?pubtool=oembed" allowfullscreen allow="autoplay"></iframe>',
            'thumbnail_url' => 'https://s2.dmcdn.net/v/Gj5BV1Zl1hN7CM6uO/x240',
            'thumbnail_width' => 427,
            'thumbnail_height' => 240,
        )*/
        $service = self::SERVICE_DAILYMOTION;

        if (!preg_match('#/video/([a-z0-9]+)(\?.*)?$#', $url, $m) || !isset($m[1]))
            return null;
        $videoId = $m[1];

        $thumbnail_filename = $this->cache_path . DIRECTORY_SEPARATOR . $service . '-' . $videoId . '.jpg';
        $thumbnail_url = $this->cache_url . '/' .  $service . '-' . $videoId . '.jpg';

        $job_args = array('service' => $service, 'videoId' => $videoId, 'thumbnail_filename' => $thumbnail_filename, 'thumbnail_url' => $data->thumbnail_url);
        //if (!as_next_scheduled_action(self::ASYNC_ACTION_DOWNLOAD_THUMBNAIL, ['job_args' => $job_args], self::PLUGIN_NAME)) {
        as_enqueue_async_action(self::ASYNC_ACTION_DOWNLOAD_THUMBNAIL, ['job_args' => $job_args], self::PLUGIN_NAME);
        //}

        // HTML to render (will be stored in oemabed WP cache)

        $title = htmlentities($data->title . ' - ' . $data->author_name, ENT_COMPAT);
        $html = ''
            . '<div class="wp-oembed-2link wp-oembed-2link-service-' . $service . '">'
            . '  <img src="' . $thumbnail_url . '" alt="' . $title . '" />'
            . '  <div class="wp-oembed-2link-menu">'
            . ' <span aria-hidden="true">' . $title . '</span><br/> '
            . '   <a class="" href="' . $url . '" target="_blank">Voir sur Dailymotion</a>'
            . ' </div>'
            . '</div>';

        return $html;
    }

    protected function process_youtube(&$url, &$data)
    {
        //error_log(__METHOD__ . ' url:' . $url);
        //error_log(__METHOD__ . ' data:' . var_export($data, true));

        /* data : (object) array(
            'title' => 'La petite histoire de Charlotte - "Pourquoi ça n\'avance pas ?" de Tomoko Ohmura',
            'author_name' => 'CSC Bléré',
            'author_url' => 'https://www.youtube.com/@cscblere2056',
            'type' => 'video',
            'height' => 608,
            'width' => 1080,
            'version' => '1.0',
            'provider_name' => 'YouTube',
            'provider_url' => 'https://www.youtube.com/',
            'thumbnail_height' => 360,
            'thumbnail_width' => 480,
            'thumbnail_url' => 'https://i.ytimg.com/vi/4LA0KfAy6ME/hqdefault.jpg',
            'html' => '<iframe width="1080" height="608" src="https://www.youtube.com/embed/4LA0KfAy6ME?feature=oembed" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen title="La petite histoire de Charlotte - &quot;Pourquoi ça n&#39;avance pas ?&quot; de Tomoko Ohmura"></iframe>',
        )*/

        $service = self::SERVICE_YOUTUBE;

        $youtube_args = null;
        parse_str(parse_url($url, PHP_URL_QUERY), $youtube_args);
        if (!is_array($youtube_args) || empty($youtube_args['v']))
            return null;
        $videoId = $youtube_args['v'];

        $thumbnail_filename = $this->cache_path . DIRECTORY_SEPARATOR . $service . '-' . $videoId . '.jpg';
        $thumbnail_url = $this->cache_url . '/' .  $service . '-' . $videoId . '.jpg';

        $job_args = array('service' => $service, 'videoId' => $videoId, 'thumbnail_filename' => $thumbnail_filename, 'thumbnail_url' => $data->thumbnail_url);
        //if (!as_next_scheduled_action(self::ASYNC_ACTION_DOWNLOAD_THUMBNAIL, ['job_args' => $job_args], self::PLUGIN_NAME)) {
        as_enqueue_async_action(self::ASYNC_ACTION_DOWNLOAD_THUMBNAIL, ['job_args' => $job_args], self::PLUGIN_NAME);
        //}

        // HTML to render (will be stored in oemabed WP cache)

        $title = htmlentities($data->title . ' - ' . $data->author_name, ENT_COMPAT);
        $html = ''
            . '<div class="wp-oembed-2link wp-oembed-2link-service-' . $service . '">'
            . '  <img src="' . $thumbnail_url . '" alt="' . $title . '" />'
            . '  <div class="wp-oembed-2link-menu">'
            . ' <span aria-hidden="true">' . $title . '</span><br/> '
            . '   <a class="" href="' . $url . '" target="_blank">Voir sur Youtube</a>'
            . ' </div>'
            . '</div>';

        return $html;
    }
}

new WpOembed2link();
